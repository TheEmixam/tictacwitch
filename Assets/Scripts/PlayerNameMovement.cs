﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNameMovement : MonoBehaviour {

    [Header("Speed")]
    public AnimationCurve _SpeedCurve;
    public float _SpeedCycleDuration;
    public Vector2 _SpeedMinMax;

    [Header("Rotation")]
    public AnimationCurve _HeightCurve;
    public float _HeightCycleDuration;
    public Vector2 _HeightMinMax;

    private float _tmpRotation = 0;
    
	// Update is called once per frame
	void Update ()
    {
        float s = Mathf.Lerp(_SpeedMinMax.x, _SpeedMinMax.y, _SpeedCurve.Evaluate(Time.time / _SpeedCycleDuration)) * Time.deltaTime;
        float h = Mathf.Lerp(_HeightMinMax.x, _HeightMinMax.y, _HeightCurve.Evaluate(Time.time / _HeightCycleDuration));

        transform.localPosition = new Vector3(0, 0, h);
        _tmpRotation += s;
        transform.parent.localRotation = Quaternion.Euler(new Vector3(0, _tmpRotation, 0));
    }
}
