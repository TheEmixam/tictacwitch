﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CanvaManager : MonoBehaviour {

    public PlayerVariable _CurrentPlayer;
    public PlayerVariable _WinnerPlayer;
    public TagVariable _CurrentTag;
    public UnityEvent _MatchStartEvent;

    public IntVariable _AmountPlayer;
    public Text _AmountPlayerText;

    [Header("Main Menu")]
    public Animator _SplashScreenAnimator;
    public Animator _MenuAnimator;
    public Button _PlayButton;

	[Header("Tuto")]
	public BoolVariable _AlreadySeen;
    public Animator _TutoPanelAnimator;
    public List<Text> _TutoTexts;
    public float _TutoTextFadeDuration;
    public float _TutoTextDuration;

    [Header("New player turn")]
    public Animator _NewPlayerTurnAnimator;
    public Text _NewPlayerTurnText;
    public PlayerHead _NewPlayerTurnHead;

    [Header("Game Panel")]
    public Animator _GamePanelAnimator;
    public Text _GamePanelPlayerNumber;
    public Text _GamePanelTag;
    public PlayerHead _GamePanelHead;

    [Header("Lose Panel")]
    public Animator _LosePanelAnimator;
    public PlayerHead _LosePanelHead;
    public Text _LosePanelText;

    [Header("Win Panel")]
    public Animator _WinPanelAnimator;
    public Text _WinPanelText;
    public PlayerHead _WinPlayerTurnHead;
    public Image _WinPanelRay;

    private bool _firstTime = true;

    private void Awake()
    {
        _SplashScreenAnimator.gameObject.SetActive(true);
        _MenuAnimator.gameObject.SetActive(true);
    }

    public void HideMenu()
    {
        _MenuAnimator.SetTrigger("Hide");
        _PlayButton.interactable = false;
		if(!_AlreadySeen.Value)
		{
			_TutoPanelAnimator.gameObject.SetActive(true);
			StartCoroutine(TutoCoroutine());
		}
		else
			_MatchStartEvent.Invoke();
	}

    IEnumerator TutoCoroutine()
    {
        foreach(Text t in _TutoTexts)
        {
            float start = Time.time;
            while(Time.time - start < _TutoTextFadeDuration)
            {
                t.color = new Color(t.color.r, t.color.g, t.color.b, (Time.time - start) / _TutoTextDuration);
                yield return null;
            }
            t.color = new Color(t.color.r, t.color.g, t.color.b, 1);

            yield return new WaitUntil(() => Input.touchCount > 0);

            start = Time.time;
            while (Time.time - start < _TutoTextFadeDuration)
            {
                t.color = new Color(t.color.r, t.color.g, t.color.b, 1 - ((Time.time - start) / _TutoTextDuration));
                yield return null;
            }
            t.color = new Color(t.color.r, t.color.g, t.color.b, 0);
        }
		_AlreadySeen.Value = true;

        _TutoPanelAnimator.SetTrigger("Fade");
        _MatchStartEvent.Invoke();
    }

    

    public void NewPlayerTurn()
    {
        StartCoroutine(NewPlayerTurnCoroutine());
        StartCoroutine(ShowGamePanelCoroutine());
    }

    public void PlayerWin()
    {
        StartCoroutine(PlayerWinCoroutine());
    }

    IEnumerator PlayerWinCoroutine()
    {
        _GamePanelAnimator.SetTrigger("Fade");
        _WinPanelText.text = (_WinnerPlayer.Value.id + 1) + "";
        _WinPlayerTurnHead.Color = _WinnerPlayer.Value.color;
        _WinPlayerTurnHead.BackHeadSprite = _WinnerPlayer.Value.head.backSprite;
        _WinPlayerTurnHead.FrontHeadSprite = _WinnerPlayer.Value.head.sprite;
        _WinPanelRay.color = _WinnerPlayer.Value.color * new Color(1,1,1,0.5f);


        yield return new WaitForSecondsRealtime(3f);
        _WinPanelAnimator.gameObject.SetActive(true);



        yield return new WaitForSecondsRealtime(6f);

        _WinPanelAnimator.SetTrigger("Fade");

        yield return new WaitForSecondsRealtime(2f);

        Application.LoadLevel("Max");
    }

    public void PlayerLose()
    {
        StartCoroutine(PlayerLoseCoroutine());
    }

    IEnumerator PlayerLoseCoroutine()
    {
        _GamePanelAnimator.SetTrigger("Fade");
        _firstTime = true;
        _LosePanelText.text = (_CurrentPlayer.Value.id + 1) + "";

        _LosePanelHead.Color= _CurrentPlayer.Value.color;
        _LosePanelHead.BackHeadSprite = _CurrentPlayer.Value.head.backSprite;
        _LosePanelHead.FrontHeadSprite = _CurrentPlayer.Value.head.sprite;

        _LosePanelAnimator.gameObject.SetActive(true);

        yield return new WaitForSecondsRealtime(2f);

        _LosePanelAnimator.SetTrigger("Fade");
    }

    private void Update()
    {
        _GamePanelPlayerNumber.text = "" + (_CurrentPlayer.Value.id + 1);

        _GamePanelHead.Color = _CurrentPlayer.Value.color;
        _GamePanelHead.BackHeadSprite = _CurrentPlayer.Value.head.backSprite;
        _GamePanelHead.FrontHeadSprite = _CurrentPlayer.Value.head.sprite;

        _GamePanelTag.text = _CurrentTag.Value;
        _AmountPlayerText.text = _AmountPlayer.Value + "";
    }

    IEnumerator NewPlayerTurnCoroutine()
    {
        _NewPlayerTurnAnimator.gameObject.SetActive(true);
        _NewPlayerTurnText.text = "" + (_CurrentPlayer.Value.id + 1);

        _NewPlayerTurnHead.Color = _CurrentPlayer.Value.color;
        _NewPlayerTurnHead.BackHeadSprite = _CurrentPlayer.Value.head.backSprite;
        _NewPlayerTurnHead.FrontHeadSprite = _CurrentPlayer.Value.head.sprite;

        yield return new WaitForSecondsRealtime(2f);

        _NewPlayerTurnAnimator.SetTrigger("Fade");
    }

    IEnumerator ShowGamePanelCoroutine()
    {
        if (!_firstTime)
            _GamePanelAnimator.SetTrigger("Fade");
        else
            _firstTime = false;

        yield return new WaitForSecondsRealtime(2.5f);

        _GamePanelAnimator.gameObject.SetActive(true);
    }
}
