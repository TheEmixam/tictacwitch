﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerHead : MonoBehaviour {

    public Image _FrontImage;
    public Image _BackImage;

    public Sprite FrontHeadSprite
    {
        set
        {
            _FrontImage.sprite = value;
        }
    }

    public Sprite BackHeadSprite
    {
        set
        {
            _BackImage.sprite = value;
        }
    }

    public Color Color
    {
        set
        {
            _BackImage.color = value;
        }
    }

}
