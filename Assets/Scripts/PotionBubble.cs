﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionBubble : MonoBehaviour {

    public Vector2 _MinMaxEmission;
    public Vector2 _MinMaxSize;


    [Header("Variable")]
    public FloatVariable _BombStart;
    public FloatVariable _BombDuration;

    private bool _isActif = false;
    private ParticleSystem _particles;
    private ParticleSystem.EmissionModule _particlesRate;
    private ParticleSystem.MainModule _particlesMain;

    private void Start()
    {
        _particles = GetComponent<ParticleSystem>();
        _particlesRate = _particles.emission;
        _particlesMain = _particles.main;
    }

    void Update()
    {
        if (_isActif)
        {
            float t = (Time.time - _BombStart.Value) / _BombDuration.Value;
            float e = Mathf.Lerp(_MinMaxEmission.x, _MinMaxEmission.y, t);
            float s = Mathf.Lerp(_MinMaxSize.x, _MinMaxSize.y, t);

            _particlesRate.rateOverTime = e;
            _particlesMain.startSize = new ParticleSystem.MinMaxCurve(0.05f, s);
        }
    }

    public void ActivateBubbles()
    {
        _isActif = true;
    }

}
