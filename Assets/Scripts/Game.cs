﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

[System.Serializable]
public struct PlayerData
{
    public int id;
    public string name;
    public Color color;
    public HeadSprite head;
}
[System.Serializable]
public struct HeadSprite
{
    public Sprite sprite;
    public Sprite backSprite;
}

public class Game : MonoBehaviour {

    public CameraInput _CameraInput;

    [Header("Timing")]
    public Vector2 MinMaxBombDurationPerPlayer;

    [Header("Events")]
    public UnityEvent OnPlayerDeath;
    public UnityEvent OnPlayerWin;
    public UnityEvent OnNewPlayerTurn;

    [Header("Variables")]
    public IntVariable _PlayerAmount;
    public IntVariable _CurrentPlayerTurn;
    public FloatVariable _BombStart;
    public FloatVariable _BombDuration;
    public PlayerVariable _CurrentPlayer;
    public TagVariable _CurrentTag;
    public PlayerVariable _Winner;

    [Header("Other")]
    public List<string> _RandomNames;
    public List<Color> _RandomColors;
    public List<HeadSprite> _RandomSprites;

    public List<PlayerData> _playersLeft;
    public Transform _ParticleAttractorPosition;
    public Transform _ParticleAttractor2Position;
	private TagManager _tagManager;

	void Start () {
        GameObject[] pTargets = GameObject.FindGameObjectsWithTag("ParticleTarget");
		foreach (GameObject g in pTargets)
        {
            g.transform.position = _ParticleAttractorPosition.position;
        }
		GameObject[] pTargets2 = GameObject.FindGameObjectsWithTag("ParticleTarget2");
		foreach (GameObject g in pTargets2)
		{
			g.transform.position = _ParticleAttractor2Position.position;
		}

		_tagManager = GetComponent<TagManager>();
        _CurrentPlayerTurn.SetValue(0);
	}

    bool PlayerDataAlreadyExist(Color color, HeadSprite head)
    {
        foreach(PlayerData p in _playersLeft)
        {
            if (p.color == color & p.head.sprite == head.sprite)
                return true;
        }
        return false;
    }

    void InitPlayers()
    {
        _playersLeft = new List<PlayerData>();
        for (int i = 0; i < _PlayerAmount.Value; i++)
        {
            PlayerData p = new PlayerData();
            p.id = i;
            int nR = Random.Range(0, _RandomNames.Count);
            //p.name = _RandomNames[nR];
            p.name = "Player " + (i + 1);

            int cR, sR;
            int trys = 0;
            do
            {
                cR = Random.Range(0, _RandomColors.Count);
                sR = Random.Range(0, _RandomSprites.Count);
                trys++;
            }
            while (PlayerDataAlreadyExist(_RandomColors[cR], _RandomSprites[sR]) && trys < 10);

            p.color = _RandomColors[cR];
            p.head = _RandomSprites[sR];

            _RandomNames.RemoveAt(nR);
            _playersLeft.Add(p);
        }
    }

    void KillPlayer(PlayerData player)
    {
        Handheld.Vibrate();

        Debug.Log(player.name + " is dead");
        _playersLeft.Remove(player);
        Time.timeScale = 1f;
        OnPlayerDeath.Invoke();
    }

    public void OnPlayerDeathEvent()
    {
        StopAllCoroutines();
        StartCoroutine(PlayerDeathCoroutine());
    }

    IEnumerator PlayerDeathCoroutine()
    {
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(3f);
        Time.timeScale = 1f;

        if(_playersLeft.Count>1)
        {
            StartCoroutine(TurnCoroutine());
            StartCoroutine(BombCoroutine());
        }
    }

    public void StartGame()
    {
        InitPlayers();
        StartCoroutine(StartGameCoroutine());
    }

    IEnumerator StartGameCoroutine()
    {
        yield return new WaitForSeconds(0.5f);

        StartCoroutine(TurnCoroutine());
        StartCoroutine(BombCoroutine());
    }

    IEnumerator BombCoroutine()
    {
        while(_playersLeft.Count > 1)
        {
            float randomTime = Random.Range(MinMaxBombDurationPerPlayer.x * _playersLeft.Count, MinMaxBombDurationPerPlayer.y * _PlayerAmount.Value);// _playersLeft.Count);

            _BombStart.SetValue(Time.time);
            _BombDuration.SetValue(randomTime);

			while (Time.time - _BombStart.Value < _BombDuration.Value)
				yield return null;

            //yield return new WaitForSeconds(randomTime);

            KillPlayer(_playersLeft[_CurrentPlayerTurn.Value]);
        }
        _Winner.SetValue(_playersLeft[0]);

        OnPlayerWin.Invoke();
        Debug.Log(_playersLeft[0].name + " has won");
    }
	
    IEnumerator TurnCoroutine()
    {
		if (Time.time - _BombStart.Value < 10f)
		{
			Debug.Log("Add duration");
			_BombStart.ApplyChange(5f);
		}


        _CurrentPlayer.SetValue(_playersLeft[_CurrentPlayerTurn.Value % _playersLeft.Count]);
        Debug.Log(_CurrentPlayer.Value.name + " start");
        OnNewPlayerTurn.Invoke();
        TagVariable tag = _tagManager.GetRandomTag();
        _CurrentTag.Value = tag.Value;

        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(3f);
        Time.timeScale = 1;

        _CameraInput.NewTurn(new List<TagVariable>{tag});

        while(!_CameraInput.AllObjectsFound)
        {
            yield return null;
        }

        _CurrentPlayerTurn.ApplyChange(1);
        _CurrentPlayerTurn.SetValue(_CurrentPlayerTurn.Value % _playersLeft.Count);

        StartCoroutine(TurnCoroutine());
    }
}
