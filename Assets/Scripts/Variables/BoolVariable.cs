﻿/*Dev Schamp Charly*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BoolVariable : ScriptableObject {
    #region public
    public bool Value
    {
        get { return m_value; }
        set { this.m_value = value; }
    }
    #endregion
    #region private
    [SerializeField]
    private bool m_value = false;
    #endregion
    #region functions
       

       
    #endregion
}