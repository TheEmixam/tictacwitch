﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;

public enum DifficultyTag
{
    Easy,
    Medium,
    Hard
}

[CreateAssetMenu]
public class TagVariable : ScriptableObject
{
    [SerializeField]
    private string value = "";

    [SerializeField]
    private DifficultyTag difficulty = DifficultyTag.Easy;

    public string Value
    {
        get { return value; }
        set { this.value = value; }
    }

    public DifficultyTag Difficulty
    {
        get { return difficulty; }
        set { this.difficulty = value; }
    }
}