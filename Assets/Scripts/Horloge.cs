﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horloge : MonoBehaviour {

    public Transform _Pendule;
    public Transform _Aiguille1;
    public Transform _Aiguille2;

    [Header("Variable")]
    public FloatVariable _BombStart;
    public FloatVariable _BombDuration;

    [Header("Pendule animation")]
    public AnimationCurve _PenduleAnimation;
    public Vector2 _PenduleSpeedMinMax;
    public Vector2 _PenduleAngleMinMax;

    [Header("AiguillesAnimation")]
    public Vector2 _AiguillesSpeedMinMax;

    private bool _isActif = false;

    void Update () {
        if(_isActif)
        {
            float t = (Time.time - _BombStart.Value) / _BombDuration.Value;
            float s = Mathf.Lerp(_PenduleSpeedMinMax.x, _PenduleSpeedMinMax.y, t);
            float angle = Mathf.Lerp(_PenduleAngleMinMax.x, _PenduleAngleMinMax.y, _PenduleAnimation.Evaluate(Mathf.Max(0, Mathf.Repeat(t * s, 1f))));
            _Pendule.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));

            float a = Mathf.Lerp(_AiguillesSpeedMinMax.x, _AiguillesSpeedMinMax.y, t) * Time.deltaTime;
            _Aiguille1.Rotate(new Vector3(0, 0, a), Space.Self);
            _Aiguille2.Rotate(new Vector3(0, 0, -a), Space.Self);
        }
    }

    public void ActivatePendule()
    {
        _isActif = true;
    }
    
}
