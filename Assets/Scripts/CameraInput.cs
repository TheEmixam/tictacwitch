﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInput : MonoBehaviour {
	public LayerMask _ClickableLayer;
    public IntVariable _AmountObjectsToFind;
    public Vector2 _CameraMinMaxZoom;
    public float _ZoomSpeedModifier;
    public float _BaseFOV;
    [HideInInspector]
    public List<TagVariable> _CurrentTags;
    [HideInInspector]
    public bool AllObjectsFound
    {
        get
        {
            return _currentObjectsFoundAmount >= _AmountObjectsToFind.Value;
        }
    }
    private int _currentObjectsFoundAmount = 0;
    private bool _gameStarted = false;

	Camera _myCam;

	// Use this for initialization
	void Start () {
		_myCam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!_gameStarted)
            return;

		if (Input.touchCount > 0)
		{
            if (Input.touchCount == 1)
            {
                Touch t = Input.GetTouch(0);
                Ray ray = _myCam.ScreenPointToRay(t.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 10000, _ClickableLayer))
                {
                    ClickableObject obj = hit.collider.GetComponent<ClickableObject>();

                    bool success = true;
                    foreach (TagVariable item in _CurrentTags)
                    {
                        if (!obj.tags.Contains(item))
                        {
                            success = false;
                            break;
                        }
                    }

                    if (success)
                    {
                        _currentObjectsFoundAmount++;
                        Debug.Log((_AmountObjectsToFind.Value - _currentObjectsFoundAmount) + " objects left to find");
                        obj.Hide();
                    }
                }
            }
            else
            {
                ZoomUpdate(Input.touches);
            }
		}
	}

    public void OnGameStart()
    {
        StartCoroutine(GameStartCoroutine());
    }

    IEnumerator GameStartCoroutine()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        _gameStarted = true;
    }

    public void NewTurn(List<TagVariable> tags)
    {
        Debug.Log("Current tag: " + tags[0].Value);
        _CurrentTags = tags;
        _currentObjectsFoundAmount = 0;
        _myCam.fov = _BaseFOV;
    }

    void ZoomUpdate(Touch[] touches)
    {
        if (touches.Length == 2)
        {
            Vector2 up, down, upPos, downPos;
            if (touches[0].position.y > touches[1].position.y)
            {
                up = touches[0].deltaPosition;
                down = touches[1].deltaPosition;
                upPos = touches[0].position;
                downPos = touches[1].position;
            }
            else
            {
                up = touches[1].deltaPosition;
                down = touches[0].deltaPosition;
                upPos = touches[1].position;
                downPos = touches[0].position;
            }
            if ((up != Vector2.zero || down != Vector2.zero) && Vector2.Dot(up, down) <= 0)
            {
                Vector2 upToDown = downPos - upPos;
                if (Vector2.Dot(upToDown, up) > 0 || Vector2.Dot(upToDown, down) < 0)
                {
                    //_myCam.fieldOfView = Mathf.Min(120f, _myCam.fieldOfView + (up.magnitude + down.magnitude) * Time.deltaTime * _ZoomSpeedModifier);
                    transform.localPosition = new Vector3(0,0, Mathf.Max(_CameraMinMaxZoom.x, transform.localPosition.z - (up.magnitude + down.magnitude) * Time.deltaTime * _ZoomSpeedModifier));
                }
                else
                {
                    //_myCam.fieldOfView = Mathf.Max(40f, _myCam.fieldOfView - (up.magnitude + down.magnitude) * Time.deltaTime * _ZoomSpeedModifier);
                    transform.localPosition = new Vector3(0,0, Mathf.Min(_CameraMinMaxZoom.y, transform.localPosition.z + (up.magnitude + down.magnitude) * Time.deltaTime * _ZoomSpeedModifier));
                }
            }
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
       // Gizmos.DrawWireSphere(transform.position, Mathf.Max(Mathf.Abs(_CameraMinMaxZoom.x), Mathf.Abs(_CameraMinMaxZoom.y)));
    }
}
