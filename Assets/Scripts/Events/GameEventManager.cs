﻿/*Dev Schamp Charly*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LumGame
{
	public class GameEventManager : MonoBehaviour {
		#region public
	  public List<GameEventListener> listGameEventListeners = new List<GameEventListener>();
    #endregion
    #region private
    #endregion
    #region functions
	  private void OnEnable()
	  {
	    foreach (var eventListener in listGameEventListeners)
	    {
	      eventListener.Event.RegisterListener(eventListener);
      } 
	  }

	  private void OnDisable()
	  {
	    foreach (var eventListener in listGameEventListeners)
	    {
	      eventListener.Event.UnregisterListener(eventListener);
	    }
	  }
    #endregion
  }
}