﻿using System.Collections;using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


	// Use this for initialization
	void Start () {
		Input.gyro.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion rotFix = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
		Quaternion rotFix2 = Quaternion.Euler(90.0f,0.0f,0.0f) * rotFix;
        transform.parent.localRotation = rotFix2;
	}
}
