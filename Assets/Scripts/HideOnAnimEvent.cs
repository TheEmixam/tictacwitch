﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOnAnimEvent : MonoBehaviour {

	public void HideOnEndAnim()
    {
        gameObject.SetActive(false);
    }
}
