﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaudron : MonoBehaviour {

    public PlayerVariable _CurrentPlayer;

    private Renderer _myRenderer;

    private void Start()
    {
        _myRenderer = GetComponent<Renderer>();
    }

    public void UpdateColor()
    {
        _myRenderer.material.SetColor("_Color", _CurrentPlayer.Value.color);
        _myRenderer.material.SetColor("_EmissionColor", _CurrentPlayer.Value.color);
    }
}
