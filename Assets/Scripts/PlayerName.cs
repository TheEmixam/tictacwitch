﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerName : MonoBehaviour {

    public PlayerVariable _CurrentPlayer;

    private TextMesh _text;

	// Use this for initialization
	void Start () {
        _text = GetComponent<TextMesh>();
	}
	
	public void UpdateName()
    {
        _text.text = _CurrentPlayer.Value.name;
    }
}
