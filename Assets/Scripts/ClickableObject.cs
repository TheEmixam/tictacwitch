﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableObject : MonoBehaviour {

	public List<TagVariable> tags;

    [HideInInspector]
    public float _TransitionDuration = 0.6f;
    [HideInInspector]
    public Vector2 _MinMaxReappearDuration = new Vector2(15f, 40f);
    [HideInInspector]
    public bool _isActif;

    [SerializeField]
    private GameObject _ParticlePrefab;

    private MeshRenderer[] _myRenderers;
    private Collider _myCollider;
    private ParticleSystem _particles;

    float Disable_Intensity
    {
        set
        {
            foreach(MeshRenderer r in _myRenderers)
            {
                r.material.SetFloat("_Disable_Object", value);
            }
        }
    }

    private void Awake()
    {
        GameObject particle = (GameObject)Instantiate(_ParticlePrefab, transform);
        particle.transform.localPosition = new Vector3(0, 0, 0);
        _particles = particle.GetComponentInChildren<ParticleSystem>();
    }

    void Start () {
        _myRenderers = GetComponentsInChildren<MeshRenderer>();
        _myCollider = GetComponent<Collider>();
        _isActif = true;
	}
	
	public void Hide()
    {
        StartCoroutine(HideCoroutine());
    }

    IEnumerator HideCoroutine()
    {
        _myCollider.enabled = false;
        _isActif = false;

		_particles.GetComponent<particleAttractorLinear>().StartParticles();
        _particles.Play();

        float startTime = Time.time;
        while(Time.time - startTime < _TransitionDuration)
        {
            Disable_Intensity = Mathf.Lerp(0f, 1f, (Time.time - startTime) / _TransitionDuration);
            yield return null;
        }
        Disable_Intensity = 1f;

        yield return new WaitForSeconds(Random.Range(_MinMaxReappearDuration.x, _MinMaxReappearDuration.y));

        startTime = Time.time;
        while (Time.time - startTime < _TransitionDuration)
        {
            Disable_Intensity = Mathf.Lerp(1f, 0f, (Time.time - startTime) / _TransitionDuration);
            yield return null;
        }

        Disable_Intensity = 0f;

        _myCollider.enabled = true;
        _isActif = true;
    }
}
