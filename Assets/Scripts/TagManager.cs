﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagManager : MonoBehaviour {
    public IntVariable _AmountObjectsToFind;
    public List<TagVariable> _Tags;

    private ClickableObject[] _objects;

    void Start () {
        _objects = GameObject.FindObjectsOfType<ClickableObject>();
	}
	
	public TagVariable GetRandomTag()
    {
        Dictionary<TagVariable, int> dictTmp = new Dictionary<TagVariable, int>();
        foreach(ClickableObject obj in _objects)
        {
            if (obj._isActif)
            {
                foreach(TagVariable tag in obj.tags)
                {
                    if (dictTmp.ContainsKey(tag))
                    {
                        dictTmp[tag]++;
                    }
                    else
                        dictTmp.Add(tag, 1);
                }
            }
        }
        List<TagVariable> listTmp = new List<TagVariable>();
        foreach(TagVariable tag in dictTmp.Keys)
        {
            if(dictTmp[tag] >= _AmountObjectsToFind.Value)
                    listTmp.Add(tag);
        }

        return listTmp[Random.Range(0, listTmp.Count)];
    }
}
