﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ColorTag
{
    public TagVariable tag;
    public Color color;
}

[RequireComponent(typeof(ClickableObject))]
public class ColorSwitcher : MonoBehaviour {

    public List<ColorTag> _Colors;

    private MeshRenderer[] _myRenderers;
    private ClickableObject _myObject;
	// Use this for initialization

    Color Color
    {
        set
        {
            foreach (MeshRenderer r in _myRenderers)
            {
                r.material.SetColor("_ColorCustom", value);
            }
        }
    }
	void Start () {
        _myRenderers = GetComponentsInChildren<MeshRenderer>();
        _myObject = GetComponent<ClickableObject>();

        ColorTag r = _Colors[Random.Range(0, _Colors.Count)];
        Color = r.color;
        _myObject.tags.Add(r.tag);
	}
}
