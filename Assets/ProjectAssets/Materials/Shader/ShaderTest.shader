// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ShaderTest"
{
	Properties
	{
		_ASEOutlineColor( "Outline Color", Color ) = (0,0,0,0)
		_ASEOutlineWidth( "Outline Width", Float ) = 0
		_Albedo("Albedo", 2D) = "white" {}
		_MainColor("MainColor", Color) = (0,0,0,0)
		_Color("Color", Color) = (0,0,0,0)
		_Color_Intensity("Color_Intensity", Range( 0 , 1)) = 0
		_Disable_Object("Disable_Object", Range( 0 , 1)) = 0
		_Intensity_Gradient("Intensity_Gradient", Range( 0.5 , 5)) = 5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog  keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		uniform fixed4 _ASEOutlineColor;
		uniform fixed _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline fixed4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return fixed4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _MainColor;
		uniform float4 _Color;
		uniform float _Color_Intensity;
		uniform float _Intensity_Gradient;
		uniform float _Disable_Object;

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 blendOpSrc3 = tex2D( _Albedo, uv_Albedo );
			float4 blendOpDest3 = _MainColor;
			float4 temp_output_3_0 = ( saturate( ( blendOpSrc3 * blendOpDest3 ) ));
			float4 blendOpSrc44 = temp_output_3_0;
			float4 blendOpDest44 = _Color;
			float4 lerpResult45 = lerp( temp_output_3_0 , ( saturate( ( blendOpSrc44 * blendOpDest44 ) )) , _Color_Intensity);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNDotV12 = dot( normalize( ase_worldNormal ), ase_worldViewDir );
			float fresnelNode12 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNDotV12, _Intensity_Gradient ) );
			float clampResult23 = clamp( fresnelNode12 , 0.0 , 1.0 );
			float4 temp_cast_0 = (( 1.0 - clampResult23 )).xxxx;
			float4 blendOpSrc20 = lerpResult45;
			float4 blendOpDest20 = temp_cast_0;
			float4 temp_output_20_0 = ( saturate( min( blendOpSrc20 , blendOpDest20 ) ));
			float4 blendOpSrc18 = temp_output_20_0;
			float4 blendOpDest18 = float4(0,0,0,0);
			float4 lerpResult17 = lerp( temp_output_20_0 , ( saturate( ( blendOpSrc18 * blendOpDest18 ) )) , ( _Disable_Object / 2 ));
			float3 desaturateVar10 = lerp( lerpResult17.rgb,dot(lerpResult17.rgb,float3(0.299,0.587,0.114)).xxx,_Disable_Object);
			o.Albedo = desaturateVar10;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers xbox360 xboxone ps4 psp2 n3ds wiiu 
		#pragma surface surf Lambert keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14301
285;92;787;407;3934.881;585.7703;3.615149;True;False
Node;AmplifyShaderEditor.CommentaryNode;35;-3980.531,-176.812;Float;False;782.897;468.1461;Albedo + Color;3;3;1;2;Albedo + Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;2;-3735.636,91.49434;Float;False;Property;_MainColor;MainColor;1;0;Create;True;0,0,0,0;0.5808823,0.5808823,0.5808823,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-3825.563,-118.3988;Float;True;Property;_Albedo;Albedo;0;0;Create;True;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;14;-2973.933,615.2238;Float;False;Property;_Intensity_Gradient;Intensity_Gradient;5;0;Create;True;5;1;0.5;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;3;-3424.974,-118.0977;Float;False;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FresnelNode;12;-2562.76,493.4288;Float;True;World;4;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;3;FLOAT;5.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;43;-3404.591,344.2075;Float;False;Property;_Color;Color;2;0;Create;True;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;23;-2246.783,510.0451;Float;False;3;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-3069.334,295.1453;Float;False;Property;_Color_Intensity;Color_Intensity;3;0;Create;True;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;44;-3020.27,27.34813;Float;False;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;45;-2670.705,-107.5727;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;34;-2340.806,-185.9725;Float;False;951.609;469.5409;Fresnel;1;20;Fresnel;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;36;-1364.187,-181.9343;Float;False;1306.634;776.0909;Disable Object;7;10;17;21;18;22;19;4;Disable Object;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;42;-2046.725,467.4202;Float;True;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;22;-1132.192,442.106;Float;False;Constant;_Int0;Int 0;4;0;Create;True;2;0;0;1;INT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-1254.601,355.0931;Float;False;Property;_Disable_Object;Disable_Object;4;0;Create;True;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;19;-1163.309,127.3283;Float;False;Constant;_Color0;Color 0;4;0;Create;True;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendOpsNode;20;-1619.635,-116.3496;Float;False;Darken;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;21;-816.99,355.7648;Float;False;2;0;FLOAT;0.0;False;1;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;18;-866.9125,-15.02274;Float;False;Multiply;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;17;-529.1672,-116.2588;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DesaturateOpNode;10;-274.1412,-117.4513;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0.0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1723.53,-105.7537;Float;False;True;2;Float;ASEMaterialInspector;0;0;Lambert;ShaderTest;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;False;0;Opaque;0;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;False;False;False;False;False;False;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;SrcAlpha;OneMinusSrcAlpha;0;One;One;OFF;OFF;0;True;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;1;0
WireConnection;3;1;2;0
WireConnection;12;3;14;0
WireConnection;23;0;12;0
WireConnection;44;0;3;0
WireConnection;44;1;43;0
WireConnection;45;0;3;0
WireConnection;45;1;44;0
WireConnection;45;2;46;0
WireConnection;42;0;23;0
WireConnection;20;0;45;0
WireConnection;20;1;42;0
WireConnection;21;0;4;0
WireConnection;21;1;22;0
WireConnection;18;0;20;0
WireConnection;18;1;19;0
WireConnection;17;0;20;0
WireConnection;17;1;18;0
WireConnection;17;2;21;0
WireConnection;10;0;17;0
WireConnection;10;1;4;0
WireConnection;0;0;10;0
ASEEND*/
//CHKSM=E3FE225E0311591AA61CA16E0851941954DCEBE9