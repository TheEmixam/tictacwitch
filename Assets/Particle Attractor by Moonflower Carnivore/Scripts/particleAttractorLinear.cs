using System.Collections;
using UnityEngine;
[RequireComponent(typeof(ParticleSystem))]
public class particleAttractorLinear : MonoBehaviour {
	ParticleSystem ps;
	ParticleSystem.Particle[] m_Particles;
	public Transform target;
	public Transform target2;
	public float heightlerpDuration;
	public float speed = 5f;
	int numParticlesAlive;
	float start;
	void Start () {
		ps = GetComponent<ParticleSystem>();
		if (!GetComponent<Transform>()){
			GetComponent<Transform>();
		}

		transform.localScale = new Vector3(1f/transform.lossyScale.x, 1f / transform.lossyScale.y, 1f / transform.lossyScale.z);
	}
	void Update () {
		m_Particles = new ParticleSystem.Particle[ps.main.maxParticles];
		numParticlesAlive = ps.GetParticles(m_Particles);
		float step = speed * Time.deltaTime;

		Vector3 p = Vector3.Lerp(target2.position, target.position, (Time.time - start) / heightlerpDuration);

		for (int i = 0; i < numParticlesAlive; i++) {
			m_Particles[i].position = Vector3.LerpUnclamped(m_Particles[i].position, p, step);
		}
		ps.SetParticles(m_Particles, numParticlesAlive);
	}

	public void StartParticles()
	{
		start = Time.time;
	}
}
